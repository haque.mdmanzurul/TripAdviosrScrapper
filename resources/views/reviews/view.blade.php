@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('reviews?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('reviews?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('reviews/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> </h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
					<tr>
						<td width='30%' class='label-view text-right'>Id</td>
						<td>{{ $row->id }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Hotel Id</td>
						<td>{{ $row->hotel_id }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>ReviewTitle</td>
						<td>{{ $row->reviewTitle }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>ReviewDescription</td>
						<td>{{ $row->reviewDescription }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>ReviewStar</td>
						<td>{{ $row->reviewStar }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>ReviewUserName</td>
						<td>{{ $row->reviewUserName }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>ReviewUserLevel</td>
						<td>{{ $row->reviewUserLevel }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>ReviewUniqueID</td>
						<td>{{ $row->reviewUniqueID }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>ReviewExtraColumn</td>
						<td>{{ $row->reviewExtraColumn }} </td>
						
					</tr>
				
		</tbody>	
	</table>   

	 
	
	</div>
</div>	

	</div>
</div>
	  
@stop