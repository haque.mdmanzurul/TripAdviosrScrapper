<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Reviews extends Sximo  {
	
	protected $table = 'tb_review';
	protected $primaryKey = 'id';

	public $timestamps = false;

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_review.* FROM tb_review  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_review.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
