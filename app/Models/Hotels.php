<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class hotels extends Sximo  {
	
	protected $table = 'tbl_hotel';
	protected $primaryKey = 'id';
        
        protected $fillable = ['hotelName', 'hotelLink', 'hotelPhone', 'hotelAddress', 'hotelImage', 'hotelRating', 'hotelRank', 'totalReviewCount', 'hotelLocationID'];
        
        public $timestamps = false;

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tbl_hotel.* FROM tbl_hotel  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tbl_hotel.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
