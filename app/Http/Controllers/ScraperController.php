<?php

namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Scraper;
use App\Models\locations;
use App\Models\hotels;
use App\Models\Reviews;
use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator,
    Input,
    Redirect,
    DB;

ini_set('max_execution_time', 300);
define('MAX_FILE_SIZE', 900000);

class ScraperController extends Controller {

    protected $layout = "layouts.main";
    protected $data = array();
    public $module = 'scraper';
    static $per_page = '10';

    public function __construct() {

        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->model = new Scraper();

        $this->info = $this->model->makeInfo($this->module);
        $this->access = $this->model->validAccess($this->info['id']);

        $this->data = array(
            'pageTitle' => $this->info['title'],
            'pageNote' => $this->info['note'],
            'pageModule' => 'scraper',
            'return' => self::returnUrl()
        );
    }

    public function getIndex(Request $request) {

        if ($this->access['is_view'] == 0)
            return Redirect::to('dashboard')
                            ->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus', 'error');

        $sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id');
        $order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
        // End Filter sort and order for query 
        // Filter Search for query		
        $filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');


        $page = $request->input('page', 1);
        $params = array(
            'page' => $page,
            'limit' => (!is_null($request->input('rows')) ? filter_var($request->input('rows'), FILTER_VALIDATE_INT) : static::$per_page ),
            'sort' => $sort,
            'order' => $order,
            'params' => $filter,
            'global' => (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
        );
        // Get Query 
        $results = $this->model->getRows($params);

        // Build pagination setting
        $page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
        $pagination = new Paginator($results['rows'], $results['total'], $params['limit']);
        $pagination->setPath('scraper');

        $this->data['rowData'] = $results['rows'];
        // Build Pagination 
        $this->data['pagination'] = $pagination;
        // Build pager number and append current param GET
        $this->data['pager'] = $this->injectPaginate();
        // Row grid Number 
        $this->data['i'] = ($page * $params['limit']) - $params['limit'];
        // Grid Configuration 
        $this->data['tableGrid'] = $this->info['config']['grid'];
        $this->data['tableForm'] = $this->info['config']['forms'];
        $this->data['colspan'] = \SiteHelpers::viewColSpan($this->info['config']['grid']);
        // Group users permission
        $this->data['access'] = $this->access;
        // Detail from master if any
        // Master detail link if any 
        $this->data['subgrid'] = (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
        // Render into template
        return view('scraper.index', $this->data);
    }

    function getUpdate(Request $request, $id = null) {

        if ($id == '') {
            if ($this->access['is_add'] == 0)
                return Redirect::to('dashboard')->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus', 'error');
        }

        if ($id != '') {
            if ($this->access['is_edit'] == 0)
                return Redirect::to('dashboard')->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus', 'error');
        }

        $row = $this->model->find($id);
        if ($row) {
            $this->data['row'] = $row;
        } else {
            $this->data['row'] = $this->model->getColumnTable('tb_category');
        }


        $this->data['id'] = $id;
        return view('scraper.form', $this->data);
    }

    public function getShow($id = null) {

        if ($this->access['is_detail'] == 0)
            return Redirect::to('dashboard')
                            ->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus', 'error');

        $row = $this->model->getRow($id);
        if ($row) {
            $this->data['row'] = $row;
        } else {
            $this->data['row'] = $this->model->getColumnTable('tb_category');
        }

        $this->data['id'] = $id;
        $this->data['access'] = $this->access;
        return view('scraper.view', $this->data);
    }

    function postSave(Request $request) {

        $rules = $this->validateForm();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $data = $this->validatePost('tb_scraper');

            $id = $this->model->insertRow($data, $request->input('id'));

            if (!is_null($request->input('apply'))) {
                $return = 'scraper/update/' . $id . '?return=' . self::returnUrl();
            } else {
                $return = 'scraper?return=' . self::returnUrl();
            }

            // Insert logs into database
            if ($request->input('id') == '') {
                \SiteHelpers::auditTrail($request, 'New Data with ID ' . $id . ' Has been Inserted !');
            } else {
                \SiteHelpers::auditTrail($request, 'Data with ID ' . $id . ' Has been Updated !');
            }

            return Redirect::to($return)->with('messagetext', \Lang::get('core.note_success'))->with('msgstatus', 'success');
        } else {

            return Redirect::to('scraper/update/' . $id)->with('messagetext', \Lang::get('core.note_error'))->with('msgstatus', 'error')
                            ->withErrors($validator)->withInput();
        }
    }

    public function postDelete(Request $request) {

        if ($this->access['is_remove'] == 0)
            return Redirect::to('dashboard')
                            ->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus', 'error');
        // delete multipe rows 
        if (count($request->input('id')) >= 1) {
            $this->model->destroy($request->input('id'));

            \SiteHelpers::auditTrail($request, "ID : " . implode(",", $request->input('id')) . "  , Has Been Removed Successfull");
            // redirect
            return Redirect::to('scraper')
                            ->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus', 'success');
        } else {
            return Redirect::to('scraper')
                            ->with('messagetext', 'No Item Deleted')->with('msgstatus', 'error');
        }
    }

    public function StoreLocation() {
        //Placed hotel names
        $url = 'https://www.tripadvisor.com';
        $html = HtmlDomParser::file_get_html($url);
        $ret = $html->find('div[class=featured wrap]');
        foreach ($ret as $div) {
            $categoryinfo = $div->find('li[class=sprite-middot]');
            foreach ($categoryinfo as $data) {
                $store = new locations();
                $store->locationName = str_replace(' Hotels', '', $data->plaintext);
                $store->locationLink = $url . $data->first_child()->href;
                //$html = HtmlDomParser::file_get_html($url . $data->first_child()->href);
                //$total_hotels = $html->find('div[class=sprite-tab-active]',0)->last_child()->plaintext;
                //echo $total_hotels;
                $store->total_hotels = 0;
                $store->processed_hotels = '0';
                $store->status = '0';
                $store->save();
            }
        }
    }

    protected function getHotels($html,$hotelLocationID,$domain){
        $ret = $html->find('div[class=listing_info]');
        foreach ($ret as $div) {
            $store = new hotels();
            $store->hotelLocationID = $hotelLocationID;
            $store->hotelName = $div->find('div[class=listing_title]', 0)->first_child()->plaintext;
            $store->hotelLink = $div->find('div[class=listing_title]', 0)->first_child()->href;

            $exist = $store::where('hotelLink', $store->hotelLink)->first();
            if(count($exist) > 0)
                continue;

            $html1 = HtmlDomParser::file_get_html("https://" . $domain . $store->hotelLink);

            // if rating is less than 3 we dont need that
            if (!empty($div->find('img[class=sprite-ratings]', 0)->alt)) {
                $store->hotelRating = str_replace(' of 5 stars', '', $div->find('img[class=sprite-ratings]', 0)->alt);
                if($store->hotelRating < 3)
                    continue;
            } else {
                $store->hotelRating = '0';
                continue;
            }
            $store->hotelAddress = $html1->find('[class=format_address]', 0)->plaintext;

            if (!empty($div->find('[class=slim_ranking]', 0)->plaintext)) {
                $rank = explode(' ', $div->find('[class=slim_ranking]', 0)->plaintext);
                $store->hotelRank = str_replace('#', '', $rank[0]);
            } else {
                $store->hotelRank = '0';
            }

            if (!empty($div->find('[class=more]', 0)->plaintext)) {
                $store->totalReviewCount = str_replace(',', '',str_replace(' Reviews', '', $div->find('[class=more]', 0)->plaintext));
            } else {
                $store->totalReviewCount = '0';
            }
            $store->save();

            $storeID = $store->id;
            $categories = array();
            foreach ($div->find('a[class=tag]') as $data) {
                $cat_exist = DB::table('tb_category')->where('categoryName',$data->plaintext)->first();
                if(count($cat_exist)>0){
                    //insert the relation with hotel
                    $tmp = array('hotelID'=>$storeID,'categoryID'=>$cat_exist->id);
                    $catrelation_insert = DB::table('tb_hotelcategory')->insert($tmp);
                }else{
                    //insert the category
                    $cat_id = DB::table('tb_category')->insertGetId(['categoryName'=>$data->plaintext]);
                    //insert the relation with hotel
                    $tmp = array('hotelID'=>$storeID,'categoryID'=>$cat_id);
                    $catrelation_insert = DB::table('tb_hotelcategory')->insert($tmp);
                }

            }
        }
    }

    public function StoreHotelInfo() {
        $allCategory = locations::where('status', 0)->get();
        //  dd($allCategory);
        foreach ($allCategory as $data) {
            $url = $data->locationLink;
            $hotelLocationID = $data->id;
            if(starts_with($url,'https://www.tripadvisor.com/VacationRentals'))
                continue;
            //get hotelinfo from category
            $domain = parse_url($url, PHP_URL_HOST);

            $html = HtmlDomParser::file_get_html($url);
            $this->getHotels($html,$hotelLocationID,$domain);
            $pages = ($html->find('div[class=pageNumbers]')->last_child()->plaintext!=null)?$html->find('div[class=pageNumbers]')->last_child()->plaintext:null;

            if($pages != null){
                $urlsegments = explode('-',$url);
                $url_list = array();
                for($i = 1; $i<$pages; $i++){
                    $pg = $i*30;
                    array_push($url_list, $urlsegments[0].'-'.$urlsegments[1].'-oa'.$pg.'-'.$urlsegments[2]);
                }

                foreach($url_list as $pg){
                    $u = 'https://' . $domain . $pg->href;
                    $html = HtmlDomParser::file_get_html($u);
                    $this->getHotels($html,$hotelLocationID,$domain);
                }
            }
           /* do {
                echo $url;
                $html = HtmlDomParser::file_get_html($url);
                $ret = $html->find('div[class=listing_info]');
                foreach ($ret as $div) {
                    $store = new hotels();
                    $store->hotelLocationID = $hotelLocationID;
                    $store->hotelName = $div->find('div[class=listing_title]', 0)->first_child()->plaintext;
                    $store->hotelLink = $div->find('div[class=listing_title]', 0)->first_child()->href;

                    $exist = $store::where('hotelLink', $store->hotelLink)->first();
                    if(count($exist) > 0)
                        continue;

                    $html1 = HtmlDomParser::file_get_html("https://" . $domain . $store->hotelLink);

                    // if rating is less than 3 we dont need that
                    if (!empty($div->find('img[class=sprite-ratings]', 0)->alt)) {
                        $store->hotelRating = str_replace(' of 5 stars', '', $div->find('img[class=sprite-ratings]', 0)->alt);
                        if($store->hotelRating < 3)
                            continue;
                    } else {
                        $store->hotelRating = '0';
                        continue;
                    }
                    $store->hotelAddress = $html1->find('[class=format_address]', 0)->plaintext;

                    if (!empty($div->find('[class=slim_ranking]', 0)->plaintext)) {
                        $rank = explode(' ', $div->find('[class=slim_ranking]', 0)->plaintext);
                        $store->hotelRank = str_replace('#', '', $rank[0]);
                    } else {
                        $store->hotelRank = '0';
                    }

                    if (!empty($div->find('[class=more]', 0)->plaintext)) {
                        $store->totalReviewCount = str_replace(',', '',str_replace(' Reviews', '', $div->find('[class=more]', 0)->plaintext));
                    } else {
                        $store->totalReviewCount = '0';
                    }
                    $store->save();

                    $storeID = $store->id;
                    $categories = array();
                    foreach ($div->find('a[class=tag]') as $data) {
                        $cat_exist = DB::table('tb_category')->where('categoryName',$data->plaintext)->first();
                        if(count($cat_exist)>0){
                            //insert the relation with hotel
                            $tmp = array('hotelID'=>$storeID,'categoryID'=>$cat_exist->id);
                            $catrelation_insert = DB::table('tb_hotelcategory')->insert($tmp);
                        }else{
                            //insert the category
                            $cat_id = DB::table('tb_category')->insertGetId(['categoryName'=>$data->plaintext]);
                            //insert the relation with hotel
                            $tmp = array('hotelID'=>$storeID,'categoryID'=>$cat_id);
                            $catrelation_insert = DB::table('tb_hotelcategory')->insert($tmp);
                        }

                    }


                }*/
                /*if ($next = $html->find('a[class=next]', 0)) {
                    $url = 'https://www.' . $domain . $next->href;
                }
                if($html->find('div[class=pagination]', 0) != null){
                */
                /*if (!empty($html->find('div[class=pagination]', 0)->childNodes(1)->href)) {
                        $url = 'https://' . $domain . $html->find('div[class=pagination]', 0)->childNodes(1)->href;
                        echo $url;
                }
                else{
                    $pagination = false;
                }*/

                //echo $html->find('a[class=next]', 0)->href;
                //$url = isset($html->find('a[class=next]', 0)->href)?$html->find('a[class=next]', 0)->href:false;



            //} while ($url);

            //echo $hotelLocationID.'-'.$page;
            $update = locations::find($hotelLocationID);
            $update->status = 1;
            $update->save();
        }
    }

    public function StoreHotelReviews() {

        $allHotels = hotels::where('status', 0)->get();;

        $initialHotelID = null;
        $pagination = true;

        foreach($allHotels as $hotel){
            //get hotel review for specific hotel
            //$url = 'https://www.tripadvisor.com/Hotel_Review-g29092-d1219838-Reviews-Anaheim_Hacienda_Inn_Suites_Disneyland-Anaheim_California.html';
            $url = 'https://www.tripadvisor.com'.$hotel->hotelLink;
            $domain = str_ireplace('www.', '', parse_url($url, PHP_URL_HOST));
            $hotelID = $hotel->id;

            do {
                $html = HtmlDomParser::file_get_html($url);
                $ret = $html->find('div[class=reviewSelector]');
                foreach ($ret as $data) {
                    $review = new Reviews();

                    $reviewStar = isset($data->find('img[class=sprite-rating_s_fill]', 0)->alt)?$data->find('img[class=sprite-rating_s_fill]', 0)->alt:'';
                    $reviewStar = str_replace(' of 5 stars', '', $reviewStar);
                    $review->reviewStar = $reviewStar;

                    if($reviewStar < 3)
                        continue;


                    $title = isset($data->find('div[class=quote]', 0)->plaintext)?$data->find('div[class=quote]', 0)->plaintext:'';
                    $review->reviewTitle = $title;

                    $link = 'https://' . $domain . $data->find('div[class=quote]', 0)->first_child()->href;
                    // unique review id
                    $urlsegment = explode('-',$link);
                    $reviewUniqueID = $urlsegment[3];
                    $check_exist = $review::where('reviewUniqueID',$reviewUniqueID)->first();
                    if(count($check_exist)>0)
                        continue;

                    $review->reviewUniqueID = $reviewUniqueID;

                    $reviewhtml = HtmlDomParser::file_get_html($link);
                    $body = $reviewhtml->find('div[class=reviewSelector]', 0)->find('div[class=entry]', 0)->first_child()->plaintext;
                    $review->reviewDescription = $body;



                    $reviewername = isset($data->find('div[class=username]', 0)->plaintext)?$data->find('div[class=username]', 0)->plaintext:'';
                    $review->reviewUserName = $reviewername;



                    $review->hotel_id = $hotelID;

                    $review->save();
                    //array_push($reviews, array('reviewUniqueID'=>$reviewUniqueID,'reviewTitle' => $title, 'reviewDescription' => $body,'reviewStar'=>$reviewStar,'reviewUserName'=>''));
                }
                if ($next = $html->find('a[class=next]', 0)) {
                    $url = 'https://www.' . $domain . $next->href;
                }
            } while ($html->find('a[class=next]', 0));

            $update = hotels::find($hotelID);
            $update->status = 1;
            $update->save();
        }

        //$result['reviews'] = $reviews;
    }

}
